import Head from 'next/head'
import Navbar from '../components/Navbar.js'
import Link from 'next/link'
import React from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import Footer from '../components/Footer'

export default function Home () {
  return (
    <>
      <Head>
        <title>GAMES ~ HOME</title>
      </Head>
      <Navbar />
      <div className="landing-page">
        <div className="container" id="section1">
          <div className="main-text">
            <div className="row best-game">
              <h1>
                WONDERFUL <span>GAME</span>
              </h1>
            </div>
            <h1>
              <span>PLAYING</span> TODAY
            </h1>
            <p>
              We Create good games experience to make you who the only one in
              this world who feels the sensation of playing a traditional game
            </p>
            <div className="button-main">
              <Link href="/login">
                <button className="play-btn">Play Now ↗</button>
              </Link>
              <Link href="/about">
                <button className="explore-btn">Explore</button>
              </Link>
            </div>
          </div>
        </div>
      </div>

        <div className='container' id='section2'>
          <div className='row align-items-center'>
            <div className='text-center'>
              <h1><span>Traditional Games</span> as Cultural Heritage</h1>
              <h1>Miss Your Childhood</h1>
              <p>Our mission to entertain the world goes beyond gaming. If you miss your childhood, our teams<br></br>create many innovative and traditional games.</p>
              <div className='video-frame'>
                <video src='/congklak-game.mp4' autoPlay loop muted />
              </div>
            </div>
          </div>
        </div>

        <div className='container' id='section3'>
          <div className='justify-align-center row'>
              <div className='text-center'>
                <h1>TO GET UPDATE <span>NEW</span> GAMES!</h1>
                <p>Subscribe to get updated on future game releases</p>
                <div className='button-scb'>
                  <div className="flexContainer">
                      <input type="text" className="form-control" name="email" placeholder="Enter Your Email" id="email-subs"/>
                      <button type="submit" className="btn btn-light"><img src='./bytesize_send.png' alt='' /></button>
                  </div>
                </div>
              </div>
          </div>
        </div>
      <Footer />
    </>
  )
}
